### photochallenge ###

* Upload photos for daily challenges

### How do I get set up? ###

* Make sure you have node.js installed
* Clone repo to the folder of your choice
* Open terminal and change your working directory
* Start project with "node start.js"

### Contribution guidelines ###

* You are not allowed to push
* All changes are made via pull requests
* Write meaningful names for pull requests\commits